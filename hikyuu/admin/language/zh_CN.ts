<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>HkuEditSessionDialog</name>
    <message>
        <location filename="../dialog/Ui_HkuEditSessionDialog.py" line="105"/>
        <source>Host/ip:</source>
        <translation>主机/IP：</translation>
    </message>
    <message>
        <location filename="../dialog/Ui_HkuEditSessionDialog.py" line="106"/>
        <source>Port:</source>
        <translation>端口：</translation>
    </message>
    <message>
        <location filename="../dialog/Ui_HkuEditSessionDialog.py" line="103"/>
        <source>User:</source>
        <translation>用户名：</translation>
    </message>
    <message>
        <location filename="../dialog/Ui_HkuEditSessionDialog.py" line="104"/>
        <source>Password:</source>
        <translation>密码：</translation>
    </message>
    <message>
        <location filename="../dialog/Ui_HkuEditSessionDialog.py" line="108"/>
        <source>Remark:</source>
        <translation>备注：</translation>
    </message>
    <message>
        <location filename="../dialog/Ui_HkuEditSessionDialog.py" line="109"/>
        <source>Test connect</source>
        <translation>测试连接</translation>
    </message>
    <message>
        <location filename="../dialog/Ui_HkuEditSessionDialog.py" line="102"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../dialog/Ui_HkuEditSessionDialog.py" line="107"/>
        <source>Name:</source>
        <translation>名称：</translation>
    </message>
    <message>
        <location filename="../dialog/HkuEditSessionDialog.py" line="56"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../dialog/HkuEditSessionDialog.py" line="32"/>
        <source>The name cannot be empty!</source>
        <translation>名称不能为空！</translation>
    </message>
    <message>
        <location filename="../dialog/HkuEditSessionDialog.py" line="49"/>
        <source>A duplicate name already exists!</source>
        <translation>已存在重复名称的会话</translation>
    </message>
</context>
<context>
    <name>HkuSessionViewWidget</name>
    <message>
        <location filename="../widget/HkuSessionViewWidget.py" line="45"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/HkuSessionViewWidget.py" line="48"/>
        <source>local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/HkuSessionViewWidget.py" line="49"/>
        <source>account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/HkuSessionViewWidget.py" line="50"/>
        <source>other</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../HikyuuAdmin.py" line="122"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation>显示Qt相关信息</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="110"/>
        <source>&amp;Quit</source>
        <translation>退出(&amp;Q)</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="110"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="129"/>
        <source>&amp;File(F)</source>
        <translation>文件(&amp;F)</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="129"/>
        <source>&amp;Help(H)</source>
        <translation>帮助(&amp;H)</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="74"/>
        <source>Hikyuu Strategy Server Manager</source>
        <translation>Hikyuu 策略服务器管理</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="174"/>
        <source>&lt;p&gt;&lt;b&gt;Hikyuu Strategy Server Manager&lt;/b&gt;&lt;p&gt;&lt;p&gt;Hikyuu strategy server management is used to manage quant trading strategies based on hikyuu quant framework&lt;/p&gt;&lt;p&gt;&lt;b&gt;Hikyuu Quant Framework&lt;/b&gt;&lt;/p&gt;It is a high performance open source quantitative trading research framework based on C++/Python, which is used for stratgy analysis and back testing.Now it only used in Chinese stock market)&lt;/p&gt;&lt;p&gt;see more: &lt;a href=&quot;https://hikyuu.org&quot;&gt;https://hikyuu.org&lt;a&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Hikyuu 策略服务器管理&lt;/b&gt;&lt;p&gt;&lt;p&gt;管理基于 Hikyuu Quant Framework 的量化交易服务器。&lt;/p&gt;&lt;p&gt;&lt;b&gt;Hikyuu Quant Framework&lt;/b&gt;&lt;/p&gt;Hikyuu Quant Framework是基于C++/Python的高性能开源量化交易研究框架，用于策略分析及回测（目前用于中国A股市场）&lt;/p&gt;&lt;p&gt;详情参见: &lt;a href=&quot;https://hikyuu.org&quot;&gt;https://hikyuu.org&lt;a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="186"/>
        <source>About Hikyuu Strategy Server Manager</source>
        <translation>关于 Hikyuu 策略服务器管理</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="110"/>
        <source>Normal style</source>
        <translation>普通风格</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="110"/>
        <source>Dark style</source>
        <translation>暗黑风格</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="121"/>
        <source>Quit Application</source>
        <translation>退出应用程序</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="129"/>
        <source>&amp;View(V)</source>
        <translation>查看(&amp;V)</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="136"/>
        <source>Skin style</source>
        <translation>显示风格</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="110"/>
        <source>About Qt</source>
        <translation>关于 Qt</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="124"/>
        <source>Switch to normal style</source>
        <translation>切换至普通显示风格</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="126"/>
        <source>Switch to dark style</source>
        <translation>切换至暗黑显示风格</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="96"/>
        <source>Running</source>
        <translation>正在运行</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="110"/>
        <source>&amp;Session</source>
        <translation>会话(&amp;S)</translation>
    </message>
    <message>
        <location filename="../HikyuuAdmin.py" line="120"/>
        <source>Session Manager</source>
        <translation>会话管理</translation>
    </message>
</context>
</TS>
